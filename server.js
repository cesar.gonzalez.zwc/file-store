#!/usr/bin/env node
'use strict';

const http = require('http');
const app = require('./app');
const port = process.env.SERVICE_PORT || 3000;

app.set('port', port);

const server = http.createServer(app);
server.listen(port);
server.on('error', error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  switch (error.code) {
    case 'EACCES':
      console.error(`Pipe ${port} requires elevated privileges`);
      process.exit(1);
      break;

    case 'EADDRINUSE':
      console.error(`Pipe ${port} requires elevated privileges`);
      process.exit(1);
      break;

    default:
      throw error;
  }
});

server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  console.log(`Listening on ${bind}`);
});
