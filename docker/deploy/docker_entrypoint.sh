#!/bin/sh
export DEBIAN_FRONTEND=noninteractive
echo "America/Santiago" > /etc/timezone
dpkg-reconfigure -f nointeractive tzdata
# [ ! -d node_modules ] && npm install
npm run start
