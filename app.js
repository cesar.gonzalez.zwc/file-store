'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const httpStatus = require('http-status-codes');
const serveStatic = require('serve-static');
const path = require('path');
const multer = require('multer');
const fs = require('fs');

const { logger, middleware: loggerMiddleware } = require('logger-js');

const upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, callback) => {
      const tenant = req.body.tenant || 'default';
      const service = req.body.service || 'default';
      const version = req.body.version || '1';
      const dir = path.join(
        __dirname,
        `/public/${tenant}/${service}/${version}`
      );
      fs.mkdir(dir, { recursive: true }, err => {
        if (err) {
          logger.error(`Unable to create directory: [${err.message}]`);

          throw err;
        }

        callback(null, dir);
      });
    },
    filename: (req, file, callback) => {
      callback(null, file.originalname);
    },
  }),
});

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(cors());
app.use(loggerMiddleware());
app.use(
  '/public',
  serveStatic(path.join(__dirname, 'public'), {
    index: false,
  })
);

logger.setLevel(process.env.LOG_LEVEL || 'INFO');

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// https://swagger.io/specification/#infoObject
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'File store',
      version: '0.0.0',
      description: 'File Store',
      contact: {
        name: 'Team',
      },
    },
  },

  apis: ['app.js'],
};

const documentation = swaggerJsDoc(swaggerOptions);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(documentation));

app.post(
  '/files',
  upload.fields([
    { name: 'tenant' },
    { name: 'service' },
    { name: 'version' },
    { name: 'file' },
  ]),
  (req, res) => {
    const tenant = req.body.tenant;
    const service = req.body.service;
    const version = req.body.version;
    const count = (req.files.file || []).length;
    const dir = path.join(__dirname, `/public/${tenant}/${service}/${version}`);

    logger.debug(`files: [%s]`, JSON.stringify(req.files));
    res.status(httpStatus.CREATED).send({
      message: `${count} files uploaded to ${dir}`,
    });
  }
);

app.get('/files*', (req, res) => {
  const subPath = (req.params || {})[0] || '';
  const dir = path.join(__dirname, `/public${subPath}`);
  fs.lstat(dir, (err, stats) => {
    if (err) {
      logger.error(`Unable to check directory: [${err.message}]`);

      return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
        message: `Unable to check directory: [${err.message}]`,
      });
    }

    if (stats.isDirectory()) {
      fs.readdir(dir, (err, files) => {
        if (err) {
          logger.error(`Unable to scan directory: [${err.message}]`);

          return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
            message: `Unable to scan directory: [${err.message}]`,
          });
        }

        return res.status(httpStatus.OK).send({
          files,
        });
      });
    } else if (stats.isFile()) {
      logger.info(`${dir} is a file. redirecting...`);

      res.writeHead(httpStatus.MOVED_TEMPORARILY, {
        Location: `/public${subPath}`,
      });
      res.end();
    } else {
      return res.status(httpStatus.NOT_FOUND).send({
        message: `${subPath} not found`,
      });
    }
  });
});

/**
 * @swagger
 * /health-check:
 *  get:
 *    produces:
 *      - application/json
 *    description: Check if the service is alive
 *    responses:
 *      '200':
 *        description: It's alive!
 */
app.get('/health-check', (req, res) =>
  res.status(httpStatus.OK).send({ alive: true })
);

/**
 * @swagger
 * /config:
 *  post:
 *    consumes:
 *      - application/json
 *    description: Change the config parameters
 *    parameters:
 *      - in: body
 *        name: body
 *        description: The body parameters
 *        required: true
 *        schema:
 *          type: object
 *          required:
 *            - log_level
 *          properties:
 *            log_level:
 *              type: string
 *              example: "info"
 *    responses:
 *      '200':
 *        description: Configuration changed
 *      '400':
 *        description: Some parameter is missing
 *      '500':
 *        description: Internal server error
 */
app.post('/config', (req, res) => {
  const logLevel = req.body['log_level'];

  if (logLevel === undefined) {
    return res.status(httpStatus.BAD_REQUEST).send({
      message: 'log_level parameter is not defined',
    });
  }

  try {
    logger.setLevel(logLevel);
    res.status(httpStatus.OK).send({
      message: `Log level changed to ${logLevel}`,
    });
  } catch (err) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY).send({
      message: err.message,
    });
  }
});

app.use((req, res) =>
  res.status(httpStatus.NOT_FOUND).send({ message: 'Not found.' })
);

module.exports = app;
