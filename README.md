# File Store

### Preparando el servicio
Levantar el contenedor
```
$ docker-compose up develop
```

Cambiar la referencia de logger-js por la que se encuentra en gitserver
```
$ docker-compose exec develop bash change_logger.sh
```

Instala las dependencias
```
$ docker-compose exec develop npm install
```

Inicia el servicio
```
$ docker-compose exec develop npm run dev
```

### Usando el servicio
#### Para cargar un listado de archivos en una estructura de archivos dada por tenant, servicio y versión
```bash
curl \
  -X POST \
  -H "Content-Type: multipart/form-data" \
  -F "tenant=default" \
  -F "service=CustomerInformation" \
  -F "version=1" \
  -F 'file=@archivo.wsdl' \
  -F 'file=@otro_archivo.xsd' \
  http://localhost:3000/files
```
La cantidad de archivos puede ser variable, en el ejemplo de arriba hay dos. Ambos archivos quedarán en una carpeta creada recursivamente dentro del proyecto. Siguiendo con el ejemplo ambos archivos estarán en:

- `./public/default/CustomerInformation/1/archivo.wsdl`
- `./public/default/CustomerInformation/1/otro_archivo.xsd`

#### Para servir el archivo previamente almacenado
```bash
curl \
  -X GET \
  http://localhost:3000/public/default/CustomerInformation/1/archivo.wsdl
```
---
:warning: :warning: :warning: **NOTE**

Ésta es la url que deberías utilizar para llamar el WSDL desde el soapClient.

---

#### Para navegar por las carpetas
Contenido de `default`
```bash
curl \
  -X GET \
  http://localhost:3000/files/default
```

Contenido de `CustomerInformation`
```bash
curl \
  -X GET \
  http://localhost:3000/files/default/CustomerInformation
```

Contenido de `1`
```bash
curl \
  -X GET \
  http://localhost:3000/files/default/CustomerInformation/1
```

Si haces finalmente algo como:
```bash
curl \
  -X GET \
  http://localhost:3000/files/default/CustomerInformation/1/archivo.wsdl
```

Y el último elemento corresponde a un archivo, el servicio hará automáticamente una redirección a 
```bash
curl \
  -X GET \
  http://localhost:3000/public/default/CustomerInformation/1/archivo.wsdl
```

Sirviendo el archivo.
