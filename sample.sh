#!/bin/bash
set -e
set -v
curl \
  -X POST \
  -H "Content-Type: multipart/form-data" \
  -F "tenant=default" \
  -F "service=CustomerInformation" \
  -F "version=1" \
  -F 'file=@server.js' \
  -F 'file=@app.js' \
  http://localhost:3000/files
