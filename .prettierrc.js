// https://prettier.io/docs/en/options.html
module.exports = {
  arrowParens: 'avoid',
  trailingComma: 'es5',
  tabWidth: 2,
  semi: true,
  singleQuote: true,
};
